import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import model.*;

@Stateless
public class Voiture {
	@PersistenceContext(name = "projet_jpa")
	EntityManager em;

    public Voiture() {
        
    }
    
    /// Couleur
    
    @SuppressWarnings("unchecked")
	public List<Couleur> getCouleurs()
    {
    		Query q = em.createQuery("select * from Couleur c");
    		
    		return q.getResultList();
    }
    
	public Couleur getCouleurById(int cid)
    {
    		Query q = em.createQuery("select * from Couleur c where c.CouleurID:cid");
    		q.setParameter("cid", cid);

    		return (Couleur) q.getSingleResult(); 
    }
    
    public void updateCouleur(Couleur c)
    {
            em.merge(c);
    }

    public void deleteCouleur(Couleur c)
    {
            em.remove(c);
    }

    public void addCouleur(Couleur c)
    {
            em.persist(c);
    }
    
    /// Jantes
    
    @SuppressWarnings("unchecked")
	public List<Jante> getJante()
    {
    		Query q = em.createQuery("select * from Jante j");
    		
    		return q.getResultList();
    }
    
	public Jante getJanteById(int jid)
    {
    		Query q = em.createQuery("select * from Jante j where j.JanteID:jid");
    		q.setParameter("jid", jid);

    		return (Jante) q.getSingleResult(); 
    }
    
    public void updateJante(Jante j)
    {
            em.merge(j);
    }

    public void deleteJante(Jante j)
    {
            em.remove(j);
    }

    public void addJante(Jante j)
    {
            em.persist(j);
    }
    
    /// FINITIONS
    
    @SuppressWarnings("unchecked")
	public List<Finition> getFinitions()
    {
    		Query q = em.createQuery("select * from Finition f");
    		
    		return q.getResultList();
    }
    
	public Finition getFinitionById(int fid)
    {
    		Query q = em.createQuery("select * from Finition f where f.FintionId:fid");
    		q.setParameter("fid", fid);

    		return (Finition) q.getSingleResult(); 
    }
    
    public void updateFinition(Finition f)
    {
            em.merge(f);
    }

    public void deleteFinition(Finition f)
    {
            em.remove(f);
    }

    public void addFinition(Finition f)
    {
            em.persist(f);
    }
    
    /// Options Supplémentaires
    
    @SuppressWarnings("unchecked")
	public List<Optionsupplementaire> getOptionSupplementaires()
    {
    		Query q = em.createQuery("select * from Optionsupplementaire os");
    		
    		return q.getResultList();
    }
    
	public Optionsupplementaire getOptionsupplementaireById(int osid)
    {
    		Query q = em.createQuery("select * from Optionsupplementaire os where os.FintionId:osid");
    		q.setParameter("osid", osid);

    		return (Optionsupplementaire) q.getSingleResult(); 
    }
    
    public void updateOptionsupplementaire(Optionsupplementaire os)
    {
            em.merge(os);
    }

    public void deleteOptionsupplementaire(Optionsupplementaire os)
    {
            em.remove(os);
    }

    public void addOptionsupplementaire(Optionsupplementaire os)
    {
            em.persist(os);
    }
    
    /// Carburants
    
    @SuppressWarnings("unchecked")
	public List<Carburant> getCarburants()
    {
    		Query q = em.createQuery("select * from Carburant c");
    		
    		return q.getResultList();
    }
    
	public Carburant getCarburantById(int cid)
    {
    		Query q = em.createQuery("select * from Carburant c where c.CarburantID:cid");
    		q.setParameter("cid", cid);

    		return (Carburant) q.getSingleResult(); 
    }
    
    public void updateCarburant(Carburant c)
    {
            em.merge(c);
    }

    public void deleteCarburant(Carburant c)
    {
            em.remove(c);
    }

    public void addCarburant(Carburant c)
    {
            em.persist(c);
    }
    
    /// Boite de vitesse
    
    @SuppressWarnings("unchecked")
	public List<Boitedevitesse> getBoitesDeVitesse()
    {
    		Query q = em.createQuery("select * from Boitedevitesse bdv");
    		
    		return q.getResultList();
    }
    
	public Boitedevitesse getBoitedevitesseById(int bdvid)
    {
    		Query q = em.createQuery("select * from Boitedevitesse bdv where bdv.BoitedevitesseID:bdvid");
    		q.setParameter("bdvid", bdvid);

    		return (Boitedevitesse) q.getSingleResult(); 
    }
    
    public void updateBoitedevitesse(Boitedevitesse bdv)
    {
            em.merge(bdv);
    }

    public void deleteBoitedevitesse(Boitedevitesse bdv)
    {
            em.remove(bdv);
    }

    public void addBoitedevitesse(Boitedevitesse bdv)
    {
            em.persist(bdv);
    }
    
    /// Transmission
    
    @SuppressWarnings("unchecked")
	public List<Transmission> getTransmissions()
    {
    		Query q = em.createQuery("select * from Transmission t");
    		
    		return q.getResultList();
    }
    
	public Transmission getTransmissionById(int tid)
    {
    		Query q = em.createQuery("select * from Transmission t where t.TransmissionID:tid");
    		q.setParameter("tid", tid);

    		return (Transmission) q.getSingleResult(); 
    }
    
    public void updateTransmission(Transmission t)
    {
            em.merge(t);
    }

    public void deleteTransmission(Transmission t)
    {
            em.remove(t);
    }

    public void addTransmission(Transmission t)
    {
            em.persist(t);
    }
    
    
    /// Motorisation
    
	@SuppressWarnings("unchecked")
	public List<Motorisation> getMotorisation()
    {
    		Query q = em.createQuery("select m from motorisation m join m.Carburant  c join m.BoiteDeVitesse bo  join m.transmission t");

    		return q.getResultList(); 
    }
	
	public Motorisation getMotorisationById(int mid)
    {
    		Query q = em.createQuery("select m from motorisation m join m.Carburant  c join m.BoiteDeVitesse bo  b join m.transmission t where m.MotorisationID= =:MotorisationID");
    		q.setParameter("mid", mid);
    	
    		return (Motorisation) q.getSingleResult(); 
    }
	
	/// Modele
	@SuppressWarnings("unchecked")
	public List<Modele> getModele()
    {
    		Query q = em.createQuery("select m from modele m \n" + 
    				"    	 join m.optionSupplementaire op\n" + 
    				"    	 join m.finitions\n" + 
    				"    	 join m.motorisation mo \n" + 
    				"    	 join mo.Carburant  c \n" + 
    				"    	 join mo.BoiteDeVitesse bo\n" + 
    				"    	 join mo.transmission t");

    		return q.getResultList(); 
    }
	
	//public Modele getModeleById(int mid)
	//{
	//		Query q = em.createQuery("select m from motorisation m join m.Carburant  c join m.BoiteDeVitesse bo  b join m.transmission t where m.MotorisationID= =:MotorisationID");
	//		q.setParameter("mid", mid);
	//	
	//		return q.getSingleResult(); 
	//}
    	 
	///Configuration
	
	@SuppressWarnings("unchecked")
	public List<Configuration> getConfiguration()
    {
    		Query q = em.createQuery("select co from configuration co\n" + 
    				"	 join co.jantes j\n" + 
    				"	 join co.couleur cou\n" + 
    				"	 \n" + 
    				"	 join co.modele m\n" + 
    				"	 join m.optionSupplementaire op\n" + 
    				"	 join m.finitions\n" + 
    				"	 \n" + 
    				"	 join m.motorisation mo \n" + 
    				"	 join mo.Carburant  c \n" + 
    				"	 join mo.BoiteDeVitesse bo\n" + 
    				"	 join mo.transmission t");

    		return q.getResultList(); 
    }
	 
	 /// Voiture
	
	@SuppressWarnings("unchecked")
	public List<Voiture> getVoiture()
    {
    		Query q = em.createQuery("select v from Voiture v \n" + 
    				"	 join v.configuration co \n" + 
    				"	 join co.jantes j\n" + 
    				"	 join co.couleur cou\n" + 
    				"	 \n" + 
    				"	 join co.modele m\n" + 
    				"	 join m.optionSupplementaire op\n" + 
    				"	 join m.finitions\n" + 
    				"	 \n" + 
    				"	 join m.motorisation mo \n" + 
    				"	 join mo.Carburant  c \n" + 
    				"	 join mo.BoiteDeVitesse bo\n" + 
    				"	 join mo.transmission t");

    		return q.getResultList(); 
    } 
	   
    
}