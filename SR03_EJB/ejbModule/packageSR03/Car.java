package packageSR03;


import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import model.*;


/**
 * Session Bean implementation class Car
 */

@Stateless
public class Car implements CarLocal {
	@PersistenceContext(name = "projet_jpa")
	EntityManager em;
    /**
     * Default constructor. 
     */
    public Car() {
        // TODO Auto-generated constructor stub
    }
    

    /// Couleur
    
    @SuppressWarnings("unchecked")
	public List<Couleur> getCouleurs()
    {
    		Query q = em.createQuery("select c from Couleur c");
    		
    		return q.getResultList();
    }
    
	public Couleur getCouleurById(int cid)
    {
    		Query q = em.createQuery("select c from Couleur c where c.idcouleur=:cid");
    		q.setParameter("cid", cid);

    		return (Couleur) q.getSingleResult(); 
    }
    
    public void updateCouleur(Couleur c)
    {
            em.merge(c);
    }

    public void deleteCouleur(Couleur c)
    {
            em.remove(c);
    }

    public void addCouleur(Couleur c)
    {
            em.persist(c);
    }
    
    /// Jantes
    
    @SuppressWarnings("unchecked")
	public List<Jante> getJantes()
    {
    		Query q = em.createQuery("select j from Jante j");
    		
    		return q.getResultList();
    }
    
	public Jante getJanteById(int jid)
    {
    		Query q = em.createQuery("select j from Jante j where j.idjante=:jid");
    		q.setParameter("jid", jid);

    		return (Jante) q.getSingleResult(); 
    }
    
    public void updateJante(Jante j)
    {
            em.merge(j);
    }

    public void deleteJante(Jante j)
    {
            em.remove(j);
    }

    public void addJante(Jante j)
    {
            em.persist(j);
    }
    
    /// FINITIONS
    
    @SuppressWarnings("unchecked")
	public List<Finition> getFinitions()
    {
    		Query q = em.createQuery("select f from Finition f");
    		
    		return q.getResultList();
    }
    
	public Finition getFinitionById(int fid)
    {
    		Query q = em.createQuery("select f from Finition f where f.finitionsid=:fid");
    		q.setParameter("fid", fid);

    		return (Finition) q.getSingleResult(); 
    }
    
    public void updateFinition(Finition f)
    {
            em.merge(f);
    }

    public void deleteFinition(Finition f)
    {
            em.remove(f);
    }

    public void addFinition(Finition f)
    {
            em.persist(f);
    }
    
    /// Options Supplémentaires
    
    @SuppressWarnings("unchecked")
	public List<Optionsupplementaire> getOptionSupplementaires()
    {
    		Query q = em.createQuery("select os from Optionsupplementaire os");
    		
    		return q.getResultList();
    }
    
	public Optionsupplementaire getOptionsupplementaireById(int osid)
    {
    		Query q = em.createQuery("select os from Optionsupplementaire os where os.idoption=:osid");
    		q.setParameter("osid", osid);

    		return (Optionsupplementaire) q.getSingleResult(); 
    }
    
    public void updateOptionsupplementaire(Optionsupplementaire os)
    {
            em.merge(os);
    }

    public void deleteOptionsupplementaire(Optionsupplementaire os)
    {
            em.remove(os);
    }

    public void addOptionsupplementaire(Optionsupplementaire os)
    {
            em.persist(os);
    }
    
    /// Carburants
    
    @SuppressWarnings("unchecked")
	public List<Carburant> getCarburants()
    {
    		Query q = em.createQuery("select c from Carburant c");
    		
    		return q.getResultList();
    }
    
	public Carburant getCarburantById(int cid)
    {
    		Query q = em.createQuery("select c from Carburant c where c.CarburantID=:cid");
    		q.setParameter("cid", cid);

    		return (Carburant) q.getSingleResult(); 
    }
    
    public void updateCarburant(Carburant c)
    {
            em.merge(c);
    }

    public void deleteCarburant(Carburant c)
    {
            em.remove(c);
    }

    public void addCarburant(Carburant c)
    {
            em.persist(c);
    }
    
    /// Boite de vitesse
    
    @SuppressWarnings("unchecked")
	public List<Boitedevitesse> getBoitesDeVitesse()
    {
    		Query q = em.createQuery("select bdv from Boitedevitesse bdv");
    		
    		return q.getResultList();
    }
    
	public Boitedevitesse getBoitedevitesseById(int bdvid)
    {
    		Query q = em.createQuery("select bdv from Boitedevitesse bdv where bdv.BoitedevitesseID=:bdvid");
    		q.setParameter("bdvid", bdvid);

    		return (Boitedevitesse) q.getSingleResult(); 
    }
    
    public void updateBoitedevitesse(Boitedevitesse bdv)
    {
            em.merge(bdv);
    }

    public void deleteBoitedevitesse(Boitedevitesse bdv)
    {
            em.remove(bdv);
    }

    public void addBoitedevitesse(Boitedevitesse bdv)
    {
            em.persist(bdv);
    }
    
    /// Transmission
    
    @SuppressWarnings("unchecked")
	public List<Transmission> getTransmissions()
    {
    		Query q = em.createQuery("select t from Transmission t");
    		
    		return q.getResultList();
    }
    
	public Transmission getTransmissionById(int tid)
    {
    		Query q = em.createQuery("select t from Transmission t where t.TransmissionID=:tid");
    		q.setParameter("tid", tid);

    		return (Transmission) q.getSingleResult(); 
    }
    
    public void updateTransmission(Transmission t)
    {
            em.merge(t);
    }

    public void deleteTransmission(Transmission t)
    {
            em.remove(t);
    }

    public void addTransmission(Transmission t)
    {
            em.persist(t);
    }
    
    
    /// Motorisation
    
	@SuppressWarnings("unchecked")
	public List<Motorisation> getMotorisations()
    {
    		Query q = em.createQuery("select m from Motorisation m join m.carburant c join m.boitedevitesse bo  join m.transmission t");

    		return q.getResultList(); 
    }
	
	public Motorisation getMotorisationById(int mid)
    {
    		Query q = em.createQuery("select m from Motorisation m join m.carburant c join m.boitedevitesse bo join m.transmission t where m.motorisationid=:mid");

    		q.setParameter("mid", mid);
    	
    		return (Motorisation) q.getSingleResult(); 
    }
	
	/// Modele
    
	@SuppressWarnings("unchecked")
	public List<Modele> getModeles()
    {
    		Query q = em.createQuery("select m from Modele m join m.finition f join m.motorisation mo join mo.carburant c join mo.boitedevitesse bo join mo.transmission t");
    		//join m.optionsupplementaires os 
    		return q.getResultList(); 
    }
	
	public Modele getModeleById(int mid)
	{
			Query q = em.createQuery("select m from Modele m join m.motorisation mo join m.finition f join m.motorisation mo join mo.carburant c join mo.boitedevitesse bdv join mo.transmission t where m.idmodele=:mid");
			q.setParameter("mid", mid);
			//join m.optionsupplementaires os
			return (Modele) q.getSingleResult(); 
	}
    	 
	///Configuration
	
	@SuppressWarnings("unchecked")
	public List<Configuration> getConfigurations()
    {
    		Query q = em.createQuery("select co from Configuration co join co.jante j join co.couleur cou join co.modele m join m.finition join m.motorisation mo join mo.carburant c join mo.boitedevitesse bo join mo.transmission t");
			//join m.optionsupplementaires os
    		return q.getResultList(); 
    }
	
	public Configuration getConfigurationById(int cid)
	{
			Query q = em.createQuery("select co from Configuration co join co.jante j join co.couleur cou join co.modele m join m.finition join m.motorisation mo join mo.carburant c join mo.boitedevitesse bo join mo.transmission t where co.idconfiguration=:cid");
			q.setParameter("cid", cid);
			//join m.optionsupplementaires os
			return (Configuration) q.getSingleResult(); 
	}
	 
	 /// Voiture
	
	@SuppressWarnings("unchecked")
	public List<Choixvoiture> getVoiture()
    {
    		//Query q = em.createQuery("select v from Choixvoiture v join v.configuration co join co.jante j join co.couleur cou join co.modele m join m.finition join m.motorisation mo join mo.carburant c join mo.boitedevitesse bo join mo.transmission t");
    		Query q = em.createQuery("select v from Choixvoiture v");

    		return q.getResultList(); 
    } 
	
	public Choixvoiture getVoitureById(int cid)
	{
			Query q = em.createQuery("select v from Choixvoiture v join v.configuration co join co.jante j join co.couleur cou join co.modele m join m.finition f join m.motorisation mo join mo.carburant c join mo.boitedevitesse bo join mo.transmission t where v.idchoixvoiture=:cid");
			q.setParameter("cid", cid);
			return (Choixvoiture) q.getSingleResult(); 
	}
	   
	
}
