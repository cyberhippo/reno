import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

@ManagedBean
@ViewScoped
public class GestJante {
	
	private List<JanteTemplate> janteTemplate;
	
	@PostConstruct
	public void init()
	{		
		this.janteTemplate = new ArrayList<JanteTemplate>();
		
		
		Client client = ClientBuilder.newClient();
		
		this.janteTemplate = client.target("http://localhost:8080/SR03_REST/voiture/jantes")
				//.queryParam("login", "hvergnol")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<JanteTemplate>>() {});
		
	}

	public List<JanteTemplate> getJanteTemplate() {
		return janteTemplate;
	}

	public void setJanteTemplate(List<JanteTemplate> janteTemplate) {
		this.janteTemplate = janteTemplate;
	}

	

}
