import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

@ManagedBean
@ViewScoped
public class GestBoitedevitesse {
	
	private List<BoitedevitesseTemplate> boitedevitesseTemplate;
	
	@PostConstruct
	public void init()
	{		
		this.boitedevitesseTemplate = new ArrayList<BoitedevitesseTemplate>();
		
		
		Client client = ClientBuilder.newClient();
		
		this.boitedevitesseTemplate = client.target("http://localhost:8080/SR03_REST/voiture/boitesdevitesse")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<BoitedevitesseTemplate>>() {});
		
	}

	public List<BoitedevitesseTemplate> getBoitedevitesseTemplate() {
		return boitedevitesseTemplate;
	}

	public void setBoitedevitesseTemplate(List<BoitedevitesseTemplate> boitedevitesseTemplate) {
		this.boitedevitesseTemplate = boitedevitesseTemplate;
	}

	

}
