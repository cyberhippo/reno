import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

@ManagedBean
@ViewScoped
public class GestTransmission {
	
	private List<TransmissionTemplate> transmissionTemplate;
	
	@PostConstruct
	public void init()
	{		
		this.setTransmissionTemplate(new ArrayList<TransmissionTemplate>());
		
		
		Client client = ClientBuilder.newClient();
		
		this.setTransmissionTemplate(client.target("http://localhost:8080/SR03_REST/voiture/transmissions")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<TransmissionTemplate>>() {}));
		
	}

	public List<TransmissionTemplate> getTransmissionTemplate() {
		return transmissionTemplate;
	}

	public void setTransmissionTemplate(List<TransmissionTemplate> transmissionTemplate) {
		this.transmissionTemplate = transmissionTemplate;
	}



	

}
