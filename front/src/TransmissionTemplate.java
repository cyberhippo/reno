
public class TransmissionTemplate {
	
	private String transmissionid;
	private String type;
	private String prix;
	
	public String getTransmissionid() {
		return transmissionid;
	}
	public void setTransmissionid(String transmissionid) {
		this.transmissionid = transmissionid;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getPrix() {
		return prix;
	}
	public void setPrix(String prix) {
		this.prix = prix;
	} 

	
}
