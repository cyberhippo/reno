public class CouleurTemplate {

	private String idcouleur;
	private String hexa;
	private String nom;
	
	public String getIdcouleur() {
		return idcouleur;
	}
	public void setIdcouleur(String idcouleur) {
		this.idcouleur = idcouleur;
	}
	public String getHexa() {
		return hexa;
	}
	public void setHexa(String hexa) {
		this.hexa = hexa;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
}
