import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

@ManagedBean
@ViewScoped
public class GestCarburant {
	
	private List<CarburantTemplate> carburantTemplate;
	
	@PostConstruct
	public void init()
	{		
		this.carburantTemplate = new ArrayList<CarburantTemplate>();
		
		
		Client client = ClientBuilder.newClient();
		
		this.carburantTemplate = client.target("http://localhost:8080/SR03_REST/voiture/carburants")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<CarburantTemplate>>() {});
		
	}

	public List<CarburantTemplate> getCarburantTemplate() {
		return carburantTemplate;
	}

	public void setCarburantTemplate(List<CarburantTemplate> carburantTemplate) {
		this.carburantTemplate = carburantTemplate;
	}

	

}
