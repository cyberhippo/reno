public class ModeleTemplate {

	private String idmodele;
	private String marque;
	private String prixbase;
	private String type;
	private FinitionTemplate finition;
	private MotorisationTemplate motorisation;
	
	public String getIdmodele() {
		return idmodele;
	}
	public void setIdmodele(String idmodele) {
		this.idmodele = idmodele;
	}
	public String getMarque() {
		return marque;
	}
	public void setMarque(String marque) {
		this.marque = marque;
	}
	public String getPrixbase() {
		return prixbase;
	}
	public void setPrixbase(String prixbase) {
		this.prixbase = prixbase;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public FinitionTemplate getFinition() {
		return finition;
	}
	public void setFinition(FinitionTemplate finition) {
		this.finition = finition;
	}
	public MotorisationTemplate getMotorisation() {
		return motorisation;
	}
	public void setMotorisation(MotorisationTemplate motorisation) {
		this.motorisation = motorisation;
	}
	
	
}
