import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;

@ManagedBean
@ViewScoped
public class GestModele {
	
	private List<ModeleTemplate> modeleTemplate;
	
	@PostConstruct
	public void init() throws JsonParseException, JsonMappingException, IOException
	{		
		this.modeleTemplate = new ArrayList<ModeleTemplate>();

		
	    String sURL = "http://localhost:8080/SR03_REST/voiture/modeles"; 

	    // Connect to the URL using java's native library
	    URL url = new URL(sURL);
	    URLConnection request = url.openConnection();
	    request.connect();

	    // Convert to a JSON object to print data
	    JsonParser jp = new JsonParser(); //from gson
	    JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element
	    JsonArray rootobj = root.getAsJsonArray(); //May be an array, may be an object. 
	    for (JsonElement el : rootobj) {
	        
	    	JsonObject ModeleObj = el.getAsJsonObject();
	        ModeleTemplate modeleTemp = new ModeleTemplate();
	        modeleTemp.setIdmodele(ModeleObj.get("idmodele").getAsString());
	        modeleTemp.setMarque(ModeleObj.get("marque").getAsString());
	        modeleTemp.setPrixbase(ModeleObj.get("prixbase").getAsString());
	        modeleTemp.setType(ModeleObj.get("type").getAsString());

	    	JsonObject FinitionObj = ModeleObj.get("finition").getAsJsonObject();
	        FinitionTemplate finitionTemp = new FinitionTemplate();
	        finitionTemp.setFinitionsid(FinitionObj.get("finitionsid").getAsString());
	        finitionTemp.setPrix(FinitionObj.get("prix").getAsString());
	        finitionTemp.setType(FinitionObj.get("type").getAsString());
	        
	    	JsonObject MotorisationObj = ModeleObj.get("motorisation").getAsJsonObject();
	        MotorisationTemplate motorisationTemp = new MotorisationTemplate();
	        motorisationTemp.setMotorisationid(MotorisationObj.get("motorisationid").getAsString());
	        motorisationTemp.setMoteur(MotorisationObj.get("moteur").getAsString());
	        motorisationTemp.setPrix(MotorisationObj.get("prix").getAsString());
	        
	        JsonObject BoiteDeViteseObj = MotorisationObj.get("boitedevitesse").getAsJsonObject();
	        BoitedevitesseTemplate boitedevitesseTemp = new BoitedevitesseTemplate();
	        boitedevitesseTemp.setBoitedevitesseid(BoiteDeViteseObj.get("boitedevitesseid").getAsString());
	        boitedevitesseTemp.setPrix(BoiteDeViteseObj.get("prix").getAsString());
	        boitedevitesseTemp.setType(BoiteDeViteseObj.get("type").getAsString());
	        
	        JsonObject carburantObj = MotorisationObj.get("carburant").getAsJsonObject();
	    	CarburantTemplate carburantTemp = new CarburantTemplate();
	    	carburantTemp.setCarburantid(carburantObj.get("carburantid").getAsString());
	    	carburantTemp.setNom(carburantObj.get("nom").getAsString());
	    	carburantTemp.setPrix(carburantObj.get("prix").getAsString());

	        JsonObject transmissionObj = MotorisationObj.get("transmission").getAsJsonObject();
	    	TransmissionTemplate transmissionTemp = new TransmissionTemplate();
	    	transmissionTemp.setTransmissionid(transmissionObj.get("transmissionid").getAsString());
	    	transmissionTemp.setPrix(transmissionObj.get("prix").getAsString());
	    	transmissionTemp.setType(transmissionObj.get("type").getAsString());

	    	motorisationTemp.setBoitedevitesse(boitedevitesseTemp);
	    	motorisationTemp.setCarburant(carburantTemp);
	    	motorisationTemp.setTransmission(transmissionTemp);
	    	
	    	modeleTemp.setFinition(finitionTemp);
	    	modeleTemp.setMotorisation(motorisationTemp);
	    	
	    	
		    System.out.printf("Prix : %s\n", modeleTemp.getMotorisation().getCarburant().getPrix());

	  
	        this.modeleTemplate.add(modeleTemp);
	    }		
	}

	public List<ModeleTemplate> getModeleTemplate() {
		return modeleTemplate;
	}

	public void setModeleTemplate(List<ModeleTemplate> modeleTemplate) {
		this.modeleTemplate = modeleTemplate;
	}

	

	

}
