public class FinitionTemplate {

	private String finitionsid;
	private String prix;
	private String type;
	
	public String getFinitionsid() {
		return finitionsid;
	}
	public void setFinitionsid(String finitionsid) {
		this.finitionsid = finitionsid;
	}
	public String getPrix() {
		return prix;
	}
	public void setPrix(String prix) {
		this.prix = prix;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
