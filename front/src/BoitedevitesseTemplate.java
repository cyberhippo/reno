
public class BoitedevitesseTemplate {
	
	private String boitedevitesseid;
	private String prix;
	private String type;
	
	public String getBoitedevitesseid() {
		return boitedevitesseid;
	}
	public void setBoitedevitesseid(String boitedevitesseid) {
		this.boitedevitesseid = boitedevitesseid;
	}
	public String getPrix() {
		return prix;
	}
	public void setPrix(String prix) {
		this.prix = prix;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	

}
