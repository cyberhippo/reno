
public class ConfigurationTemplate {
	
	private String idconfiguration;
	private String url;
	private CouleurTemplate couleur;
	private JanteTemplate jante;
	private ModeleTemplate modele;
	
	public String getIdconfiguration() {
		return idconfiguration;
	}
	public void setIdconfiguration(String idconfiguration) {
		this.idconfiguration = idconfiguration;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public CouleurTemplate getCouleur() {
		return couleur;
	}
	public void setCouleur(CouleurTemplate couleur) {
		this.couleur = couleur;
	}
	public JanteTemplate getJante() {
		return jante;
	}
	public void setJante(JanteTemplate jante) {
		this.jante = jante;
	}
	public ModeleTemplate getModele() {
		return modele;
	}
	public void setModele(ModeleTemplate modele) {
		this.modele = modele;
	}
	

}
