import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;

@ManagedBean
@ViewScoped
public class GestMotorisation {
	
	private List<MotorisationTemplate> motorisationTemplate;
	
	@PostConstruct
	public void init() throws JsonParseException, JsonMappingException, IOException
	{		
		this.motorisationTemplate = new ArrayList<MotorisationTemplate>();

		
	    String sURL = "http://localhost:8080/SR03_REST/voiture/motorisations"; 

	    // Connect to the URL using java's native library
	    URL url = new URL(sURL);
	    URLConnection request = url.openConnection();
	    request.connect();

	    // Convert to a JSON object to print data
	    JsonParser jp = new JsonParser(); //from gson
	    JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element
	    JsonArray rootobj = root.getAsJsonArray(); //May be an array, may be an object. 
	    for (JsonElement el : rootobj) {
	        JsonObject MotorisationObj = el.getAsJsonObject();
	        MotorisationTemplate temp = new MotorisationTemplate();
	        temp.setMotorisationid(MotorisationObj.get("motorisationid").getAsString());
	        temp.setMoteur(MotorisationObj.get("moteur").getAsString());
	        temp.setPrix(MotorisationObj.get("prix").getAsString());
	        
	        JsonObject BoiteDeViteseObj = MotorisationObj.get("boitedevitesse").getAsJsonObject();
	        BoitedevitesseTemplate boitedevitesseTemp = new BoitedevitesseTemplate();
	        boitedevitesseTemp.setBoitedevitesseid(BoiteDeViteseObj.get("boitedevitesseid").getAsString());
	        boitedevitesseTemp.setPrix(BoiteDeViteseObj.get("prix").getAsString());
	        boitedevitesseTemp.setType(BoiteDeViteseObj.get("type").getAsString());
	        
	        JsonObject carburantObj = MotorisationObj.get("carburant").getAsJsonObject();
	    	CarburantTemplate carburantTemp = new CarburantTemplate();
	    	carburantTemp.setCarburantid(carburantObj.get("carburantid").getAsString());
	    	carburantTemp.setNom(carburantObj.get("nom").getAsString());
	    	carburantTemp.setPrix(carburantObj.get("prix").getAsString());

	        JsonObject transmissionObj = MotorisationObj.get("transmission").getAsJsonObject();
	    	TransmissionTemplate transmissionTemp = new TransmissionTemplate();
	    	transmissionTemp.setTransmissionid(transmissionObj.get("transmissionid").getAsString());
	    	transmissionTemp.setPrix(transmissionObj.get("prix").getAsString());
	    	transmissionTemp.setType(transmissionObj.get("type").getAsString());

	    	temp.setBoitedevitesse(boitedevitesseTemp);
	    	temp.setCarburant(carburantTemp);
	    	temp.setTransmission(transmissionTemp);
	    	
		    //System.out.printf("Prix : %s\n", prix);
		    //String     dateEntered = paymentObj.get("date_entered").getAsString();
	        //BigDecimal amount      = paymentObj.get("amount").getAsBigDecimal();
		    
	        this.motorisationTemplate.add(temp);
	    }		
	}

	public List<MotorisationTemplate> getMotorisationTemplate() {
		return motorisationTemplate;
	}

	public void setMotorisationTemplate(List<MotorisationTemplate> motorisationTemplate) {
		this.motorisationTemplate = motorisationTemplate;
	}

	

}
