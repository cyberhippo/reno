import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.*;

@ManagedBean
@ViewScoped
public class GestDetails {
	
	HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();   
    
	private String idconfig = (String)req.getParameter("id");    
    
	private ConfigurationTemplate config;
	
	@PostConstruct
	public void init() throws JsonParseException, JsonMappingException, IOException
	{		
		this.setConfig(new ConfigurationTemplate());

		
	    String sURL = "http://localhost:8080/SR03_REST/voiture/configuration/" + idconfig; 

	    // Connect to the URL using java's native library
	    URL url = new URL(sURL);
	    URLConnection request = url.openConnection();
	    request.connect();

	    // Convert to a JSON object to print data
	    JsonParser jp = new JsonParser(); //from gson
	    JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element
	    JsonObject rootobj = root.getAsJsonObject(); //May be an array, may be an object. 
	    //for (JsonElement el : rootobj) {
	        
	    	//JsonObject ConfigurationObj = el.getAsJsonObject();
	    	JsonObject ConfigurationObj = rootobj;
	        ConfigurationTemplate configurationTemp = new ConfigurationTemplate();
	        configurationTemp.setIdconfiguration(ConfigurationObj.get("idconfiguration").getAsString());
	        configurationTemp.setUrl(ConfigurationObj.get("url").getAsString());

	    	JsonObject CouleurObj = ConfigurationObj.get("couleur").getAsJsonObject();
	        CouleurTemplate couleurTemp = new CouleurTemplate();
	        couleurTemp.setIdcouleur(CouleurObj.get("idcouleur").getAsString());
	        couleurTemp.setHexa(CouleurObj.get("hexa").getAsString());
	        couleurTemp.setNom(CouleurObj.get("nom").getAsString());
	        
	        JsonObject JanteObj = ConfigurationObj.get("jante").getAsJsonObject();
	        JanteTemplate janteTemp = new JanteTemplate();
	        janteTemp.setIdjante(JanteObj.get("idjante").getAsString());
	        janteTemp.setPrix(JanteObj.get("prix").getAsString());
	        janteTemp.setType(JanteObj.get("type").getAsString());
	        
	    	JsonObject ModeleObj = ConfigurationObj.get("modele").getAsJsonObject();
	        ModeleTemplate modeleTemp = new ModeleTemplate();
	        modeleTemp.setIdmodele(ModeleObj.get("idmodele").getAsString());
	        modeleTemp.setMarque(ModeleObj.get("marque").getAsString());
	        modeleTemp.setPrixbase(ModeleObj.get("prixbase").getAsString());
	        modeleTemp.setType(ModeleObj.get("type").getAsString());

	    	JsonObject FinitionObj = ModeleObj.get("finition").getAsJsonObject();
	        FinitionTemplate finitionTemp = new FinitionTemplate();
	        finitionTemp.setFinitionsid(FinitionObj.get("finitionsid").getAsString());
	        finitionTemp.setPrix(FinitionObj.get("prix").getAsString());
	        finitionTemp.setType(FinitionObj.get("type").getAsString());
	        
	    	JsonObject MotorisationObj = ModeleObj.get("motorisation").getAsJsonObject();
	        MotorisationTemplate motorisationTemp = new MotorisationTemplate();
	        motorisationTemp.setMotorisationid(MotorisationObj.get("motorisationid").getAsString());
	        motorisationTemp.setMoteur(MotorisationObj.get("moteur").getAsString());
	        motorisationTemp.setPrix(MotorisationObj.get("prix").getAsString());
	        
	        JsonObject BoiteDeViteseObj = MotorisationObj.get("boitedevitesse").getAsJsonObject();
	        BoitedevitesseTemplate boitedevitesseTemp = new BoitedevitesseTemplate();
	        boitedevitesseTemp.setBoitedevitesseid(BoiteDeViteseObj.get("boitedevitesseid").getAsString());
	        boitedevitesseTemp.setPrix(BoiteDeViteseObj.get("prix").getAsString());
	        boitedevitesseTemp.setType(BoiteDeViteseObj.get("type").getAsString());
	        
	        JsonObject carburantObj = MotorisationObj.get("carburant").getAsJsonObject();
	    	CarburantTemplate carburantTemp = new CarburantTemplate();
	    	carburantTemp.setCarburantid(carburantObj.get("carburantid").getAsString());
	    	carburantTemp.setNom(carburantObj.get("nom").getAsString());
	    	carburantTemp.setPrix(carburantObj.get("prix").getAsString());

	        JsonObject transmissionObj = MotorisationObj.get("transmission").getAsJsonObject();
	    	TransmissionTemplate transmissionTemp = new TransmissionTemplate();
	    	transmissionTemp.setTransmissionid(transmissionObj.get("transmissionid").getAsString());
	    	transmissionTemp.setPrix(transmissionObj.get("prix").getAsString());
	    	transmissionTemp.setType(transmissionObj.get("type").getAsString());

	    	motorisationTemp.setBoitedevitesse(boitedevitesseTemp);
	    	motorisationTemp.setCarburant(carburantTemp);
	    	motorisationTemp.setTransmission(transmissionTemp);
	    	
	    	modeleTemp.setFinition(finitionTemp);
	    	modeleTemp.setMotorisation(motorisationTemp);
	    	
	    	configurationTemp.setCouleur(couleurTemp);
	    	configurationTemp.setJante(janteTemp);
	    	configurationTemp.setModele(modeleTemp);
	  
	    	
	        this.config = configurationTemp;
	    //}		
	}


	public ConfigurationTemplate getConfig() {
		return config;
	}

	public void setConfig(ConfigurationTemplate config) {
		this.config = config;
	}

}
