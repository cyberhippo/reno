
public class CarburantTemplate {
	
	private String carburantid;
	private String nom;
	private String prix;
	
	public String getCarburantid() {
		return carburantid;
	}
	public void setCarburantid(String carburantid) {
		this.carburantid = carburantid;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrix() {
		return prix;
	}
	public void setPrix(String prix) {
		this.prix = prix;
	}
}
