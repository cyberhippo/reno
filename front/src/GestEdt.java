import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

@ManagedBean
@ViewScoped
public class GestEdt {
	
	private List<EdtTemplate> edtTemplate;
	
	@PostConstruct
	public void init()
	{		
		this.edtTemplate = new ArrayList<EdtTemplate>();
		
		
		Client client = ClientBuilder.newClient();
		
		this.edtTemplate = client.target("https://webapplis.utc.fr/Edt_ent_rest/myedt/result")
				.queryParam("login", "hvergnol")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<EdtTemplate>>() {});
		
	}

	public List<EdtTemplate> getEdtTemplate() {
		return edtTemplate;
	}

	public void setEdtTemplate(List<EdtTemplate> edtTemplate) {
		this.edtTemplate = edtTemplate;
	}

	

}
