public class MotorisationTemplate {

	private String motorisationid;
	private String moteur;
	private String prix;
	private BoitedevitesseTemplate boitedevitesse;
	private CarburantTemplate carburant;
	private TransmissionTemplate transmission;
	
	public String getMotorisationid() {
		return motorisationid;
	}
	public void setMotorisationid(String motorisationid) {
		this.motorisationid = motorisationid;
	}
	public String getMoteur() {
		return moteur;
	}
	public void setMoteur(String moteur) {
		this.moteur = moteur;
	}
	public String getPrix() {
		return prix;
	}
	public void setPrix(String prix) {
		this.prix = prix;
	}
	public BoitedevitesseTemplate getBoitedevitesse() {
		return boitedevitesse;
	}
	public void setBoitedevitesse(BoitedevitesseTemplate boitedevitesse) {
		this.boitedevitesse = boitedevitesse;
	}
	public CarburantTemplate getCarburant() {
		return carburant;
	}
	public void setCarburant(CarburantTemplate carburant) {
		this.carburant = carburant;
	}
	public TransmissionTemplate getTransmission() {
		return transmission;
	}
	public void setTransmission(TransmissionTemplate transmission) {
		this.transmission = transmission;
	}
	

}
