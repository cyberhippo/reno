import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

@ManagedBean
@ViewScoped
public class GestCouleur {
	
	private List<CouleurTemplate> couleurTemplate;
	
	@PostConstruct
	public void init()
	{		
		this.couleurTemplate = new ArrayList<CouleurTemplate>();
		
		
		Client client = ClientBuilder.newClient();
		
		this.setCouleurTemplate(client.target("http://localhost:8080/SR03_REST/voiture/couleurs")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<CouleurTemplate>>() {}));
		
	}

	public List<CouleurTemplate> getCouleurTemplate() {
		return couleurTemplate;
	}

	public void setCouleurTemplate(List<CouleurTemplate> couleurTemplate) {
		this.couleurTemplate = couleurTemplate;
	}
	

}
