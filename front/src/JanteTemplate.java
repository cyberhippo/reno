public class JanteTemplate {
	
	private String idjante;
	private String prix;
	private String type;
	
	public String getIdjante() {
		return idjante;
	}
	public void setIdjante(String idjante) {
		this.idjante = idjante;
	}
	public String getPrix() {
		return prix;
	}
	public void setPrix(String prix) {
		this.prix = prix;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
