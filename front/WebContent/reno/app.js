$(document).ready(function () {
    $("#motorisationButton").click(function () {
    	$("#motorisationDiv").css("display", 'flex');
        $("#configurationDiv").css("display", 'none');
        $("#modeleDiv").css("display", 'none');
        console.log("motorisation clicked..");        
    });
    $("#configurationButton").click(function () {
        $("#motorisationDiv").css("display", 'none');
        $("#configurationDiv").css("display", 'flex');
        $("#modeleDiv").css("display", 'none');
    });
    $("#modeleButton").click(function () {
        $("#motorisationDiv").css("display", 'none');
        $("#configurationDiv").css("display", 'none');
        $("#modeleDiv").css("display", 'flex');
    });

    $("input:radio").click(function (res) {
        var prix = 0; 
        $("input:radio:checked").each(function( index ) {
            prix+=Number($( this ).attr("prix"))
        });
        $("#carPriceDiv").text("PRIX : "+prix+"€");
    });
    
 });