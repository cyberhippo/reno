drop database if exists RENO;

create database RENO;

\connect reno;

create table Jantes (
    idJante int NOT NULL PRIMARY KEY,
    type varchar(255),
    prix int

);

create table Couleur (
    idCouleur int NOT NULL PRIMARY KEY,
    nom varchar(255),
    hexa varchar(7)
);

create table OptionSupplementaire (
    idOption int NOT NULL PRIMARY KEY,
    nom varchar(255),
    prix int
);

CREATE TABLE Carburant (
    CarburantID INT NOT NULL,
    PRIMARY KEY (CarburantID),
    Nom varchar(255) NOT NULL,
    Prix integer NOT NULL
);

CREATE TABLE BoiteDeVitesse (
    BoiteDeVitesseID int NOT NULL,
    Type varchar(255) NOT NULL,
    Prix integer NOT NULL,
    PRIMARY KEY (BoiteDeVitesseID)
);

CREATE TABLE Transmission (
    TransmissionID int NOT NULL,
    Type varchar(255) NOT NULL,
    Prix integer NOT NULL,
    PRIMARY KEY (TransmissionID)
);

CREATE TABLE Finitions (
    FinitionsID int NOT NULL,
    Type varchar(255) NOT NULL,
    Prix integer NOT NULL,
    PRIMARY KEY (FinitionsID)
);



CREATE TABLE Motorisation (
    MotorisationID int NOT NULL,
    PRIMARY KEY (MotorisationID),
    Moteur varchar(255) NOT NULL,
    Prix integer NOT NULL,
    CarburantID int,
    BoiteDeVitesseID int,
    TransmissionID int,
    FOREIGN KEY (CarburantID) REFERENCES Carburant(CarburantID),
    FOREIGN KEY (BoiteDeVitesseID) REFERENCES
BoiteDeVitesse(BoiteDeVitesseID),
    FOREIGN KEY (TransmissionID) REFERENCES Transmission(TransmissionID)
);


create table Modele (
    idModele int NOT NULL PRIMARY KEY,
    prixBase float,
    type varchar(255),
    marque varchar(255),
    MotorisationID int,
    FinitionsID int,
    FOREIGN KEY (MotorisationID) REFERENCES
Motorisation(MotorisationID),
    FOREIGN KEY (FinitionsID) REFERENCES Finitions(FinitionsID)
);

create table AjouteOption (
    idModele int NOT NULL,
    idOption int NOT NULL,
    PRIMARY KEY(idModele,idOption),
    FOREIGN KEY (idModele) REFERENCES Modele(idModele),
    FOREIGN KEY (idOption) REFERENCES OptionSupplementaire(idOption)
);

create table Configuration (
    idConfiguration int NOT NULL PRIMARY KEY,
    idJante int,
    idCouleur int,
    idModele int,
    url varchar(255) NOT NULL,
    FOREIGN KEY (idJante) REFERENCES Jantes(idJante),
    FOREIGN KEY (idCouleur) REFERENCES Couleur(idCouleur),
    FOREIGN KEY (idModele) REFERENCES Modele(idModele)
);

create table ChoixVoiture (
    idChoixVoiture int NOT NULL PRIMARY KEY,
    prix int,
    idConfiguration int,
    FOREIGN KEY (idConfiguration) REFERENCES
Configuration(idConfiguration)
);
