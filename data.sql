\connect reno;

INSERT INTO couleur VALUES(1,'rouge','#FF0000');
INSERT INTO couleur VALUES(2,'bleu','#0000FF');
INSERT INTO couleur VALUES(3,'orange','#FFA500');
INSERT INTO couleur VALUES(4,'gris','#C0C0C0');

INSERT INTO jantes VALUES(1,'jante en tôle d’acier',2000);
INSERT INTO jantes VALUES(2,'jante en alu',3000);
INSERT INTO jantes VALUES(3,'jante en alliage',5000);

INSERT INTO carburant VALUES(1,'Gazole',1000);
INSERT INTO carburant VALUES(2,'SP95',1100);
INSERT INTO carburant VALUES(3,'E85',1250);
INSERT INTO carburant VALUES(4,'GPL',7500);
INSERT INTO carburant VALUES(5,'E10',500);

INSERT INTO boitedevitesse VALUES(1,'Boîte manuelle',3500);
INSERT INTO boitedevitesse VALUES(2,'Boîte à double embrayage',4500);
INSERT INTO boitedevitesse VALUES(3,'Boîte séquentielle',2500);
INSERT INTO boitedevitesse VALUES(4,'Boîte automatique',6500);

INSERT INTO transmission VALUES(1,'Traction',8500);
INSERT INTO transmission VALUES(2,'Propulsion',9500);
INSERT INTO transmission VALUES(3,'4x4',15500);

INSERT INTO motorisation VALUES(1,'2.0 TFSI',60000,1,1,2);
INSERT INTO motorisation VALUES(2,'DCI 11',10000,1,1,2);
INSERT INTO motorisation VALUES(3,'318i 136 ch',30000,1,1,2);
INSERT INTO motorisation VALUES(4,'1.2l Puretech turbo',45000,1,1,2);

INSERT INTO finitions VALUES(1,'Equipement de série',5000);
INSERT INTO finitions VALUES(2,'Finitions Life',7000);
INSERT INTO finitions VALUES(3,'Finitions Zen',9000);
INSERT INTO finitions VALUES(4,'Finitions Bose',12000);

INSERT INTO modele VALUES(1, 10000, 'R8', 'AUDI', 1,1);
INSERT INTO modele VALUES(2, 10000, 'R8', 'BMW', 1,1);
INSERT INTO modele VALUES(3, 10000, 'A7', 'AUDI', 1,1);

INSERT INTO configuration VALUES(1,2,1,1,'https://icdn-4.motor1.com/images/mgl/2mJwE/s4/2018-audi-a7-sportback.jpg');
