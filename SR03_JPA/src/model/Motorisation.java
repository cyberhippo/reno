package model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the motorisation database table.
 * 
 */
@Entity
@NamedQuery(name="Motorisation.findAll", query="SELECT m FROM Motorisation m")
public class Motorisation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer motorisationid;

	private String moteur;

	private Integer prix;

	//bi-directional many-to-one association to Modele
	@JsonIgnore
	@OneToMany(mappedBy="motorisation")
	private List<Modele> modeles;

	//bi-directional many-to-one association to Boitedevitesse
	@ManyToOne
	@JoinColumn(name="boitedevitesseid")
	private Boitedevitesse boitedevitesse;

	//bi-directional many-to-one association to Carburant
	@ManyToOne
	@JoinColumn(name="carburantid")
	private Carburant carburant;

	//bi-directional many-to-one association to Transmission
	@ManyToOne
	@JoinColumn(name="transmissionid")
	private Transmission transmission;

	public Motorisation() {
	}

	public Integer getMotorisationid() {
		return this.motorisationid;
	}

	public void setMotorisationid(Integer motorisationid) {
		this.motorisationid = motorisationid;
	}

	public String getMoteur() {
		return this.moteur;
	}

	public void setMoteur(String moteur) {
		this.moteur = moteur;
	}

	public Integer getPrix() {
		return this.prix;
	}

	public void setPrix(Integer prix) {
		this.prix = prix;
	}

	public List<Modele> getModeles() {
		return this.modeles;
	}

	public void setModeles(List<Modele> modeles) {
		this.modeles = modeles;
	}

	public Modele addModele(Modele modele) {
		getModeles().add(modele);
		modele.setMotorisation(this);

		return modele;
	}

	public Modele removeModele(Modele modele) {
		getModeles().remove(modele);
		modele.setMotorisation(null);

		return modele;
	}

	public Boitedevitesse getBoitedevitesse() {
		return this.boitedevitesse;
	}

	public void setBoitedevitesse(Boitedevitesse boitedevitesse) {
		this.boitedevitesse = boitedevitesse;
	}

	public Carburant getCarburant() {
		return this.carburant;
	}

	public void setCarburant(Carburant carburant) {
		this.carburant = carburant;
	}

	public Transmission getTransmission() {
		return this.transmission;
	}

	public void setTransmission(Transmission transmission) {
		this.transmission = transmission;
	}

}