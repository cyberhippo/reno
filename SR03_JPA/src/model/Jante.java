package model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the jantes database table.
 * 
 */
@Entity
@Table(name="jantes")
@NamedQuery(name="Jante.findAll", query="SELECT j FROM Jante j")
public class Jante implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer idjante;

	private Integer prix;

	private String type;

	//bi-directional many-to-one association to Configuration
	@JsonIgnore
	@OneToMany(mappedBy="jante")
	private List<Configuration> configurations;

	public Jante() {
	}

	public Integer getIdjante() {
		return this.idjante;
	}

	public void setIdjante(Integer idjante) {
		this.idjante = idjante;
	}

	public Integer getPrix() {
		return this.prix;
	}

	public void setPrix(Integer prix) {
		this.prix = prix;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Configuration> getConfigurations() {
		return this.configurations;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}

	public Configuration addConfiguration(Configuration configuration) {
		getConfigurations().add(configuration);
		configuration.setJante(this);

		return configuration;
	}

	public Configuration removeConfiguration(Configuration configuration) {
		getConfigurations().remove(configuration);
		configuration.setJante(null);

		return configuration;
	}

}