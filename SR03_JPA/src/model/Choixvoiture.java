package model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the choixvoiture database table.
 * 
 */
@Entity
@NamedQuery(name="Choixvoiture.findAll", query="SELECT c FROM Choixvoiture c")
public class Choixvoiture implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer idchoixvoiture;

	private Integer prix;

	//bi-directional many-to-one association to Configuration
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="idconfiguration")
	private Configuration configuration;

	public Choixvoiture() {
	}

	public Integer getIdchoixvoiture() {
		return this.idchoixvoiture;
	}

	public void setIdchoixvoiture(Integer idchoixvoiture) {
		this.idchoixvoiture = idchoixvoiture;
	}

	public Integer getPrix() {
		return this.prix;
	}

	public void setPrix(Integer prix) {
		this.prix = prix;
	}

	public Configuration getConfiguration() {
		return this.configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

}