package model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the boitedevitesse database table.
 * 
 */
@Entity
@NamedQuery(name="Boitedevitesse.findAll", query="SELECT b FROM Boitedevitesse b")
public class Boitedevitesse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer boitedevitesseid;

	private Integer prix;

	private String type;

	//bi-directional many-to-one association to Motorisation
	@JsonIgnore
	@OneToMany(mappedBy="boitedevitesse")
	private List<Motorisation> motorisations;

	public Boitedevitesse() {
	}

	public Integer getBoitedevitesseid() {
		return this.boitedevitesseid;
	}

	public void setBoitedevitesseid(Integer boitedevitesseid) {
		this.boitedevitesseid = boitedevitesseid;
	}

	public Integer getPrix() {
		return this.prix;
	}

	public void setPrix(Integer prix) {
		this.prix = prix;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Motorisation> getMotorisations() {
		return this.motorisations;
	}

	public void setMotorisations(List<Motorisation> motorisations) {
		this.motorisations = motorisations;
	}

	public Motorisation addMotorisation(Motorisation motorisation) {
		getMotorisations().add(motorisation);
		motorisation.setBoitedevitesse(this);

		return motorisation;
	}

	public Motorisation removeMotorisation(Motorisation motorisation) {
		getMotorisations().remove(motorisation);
		motorisation.setBoitedevitesse(null);

		return motorisation;
	}

}