package model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the couleur database table.
 * 
 */
@Entity
@NamedQuery(name="Couleur.findAll", query="SELECT c FROM Couleur c")
public class Couleur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer idcouleur;

	private String hexa;

	private String nom;

	//bi-directional many-to-one association to Configuration
	@JsonIgnore
	@OneToMany(mappedBy="couleur")
	private List<Configuration> configurations;

	public Couleur() {
	}

	public Integer getIdcouleur() {
		return this.idcouleur;
	}

	public void setIdcouleur(Integer idcouleur) {
		this.idcouleur = idcouleur;
	}

	public String getHexa() {
		return this.hexa;
	}

	public void setHexa(String hexa) {
		this.hexa = hexa;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Configuration> getConfigurations() {
		return this.configurations;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}

	public Configuration addConfiguration(Configuration configuration) {
		getConfigurations().add(configuration);
		configuration.setCouleur(this);

		return configuration;
	}

	public Configuration removeConfiguration(Configuration configuration) {
		getConfigurations().remove(configuration);
		configuration.setCouleur(null);

		return configuration;
	}

}