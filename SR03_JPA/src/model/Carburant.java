package model;

import java.io.Serializable;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;


/**
 * The persistent class for the carburant database table.
 * 
 */
@Entity
@NamedQuery(name="Carburant.findAll", query="SELECT c FROM Carburant c")
public class Carburant implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer carburantid;

	private String nom;

	private Integer prix;

	//bi-directional many-to-one association to Motorisation
	@JsonIgnore
	@OneToMany(mappedBy="carburant")
	private List<Motorisation> motorisations;

	public Carburant() {
	}

	public Integer getCarburantid() {
		return this.carburantid;
	}

	public void setCarburantid(Integer carburantid) {
		this.carburantid = carburantid;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getPrix() {
		return this.prix;
	}

	public void setPrix(Integer prix) {
		this.prix = prix;
	}

	public List<Motorisation> getMotorisations() {
		return this.motorisations;
	}

	public void setMotorisations(List<Motorisation> motorisations) {
		this.motorisations = motorisations;
	}

	public Motorisation addMotorisation(Motorisation motorisation) {
		getMotorisations().add(motorisation);
		motorisation.setCarburant(this);

		return motorisation;
	}

	public Motorisation removeMotorisation(Motorisation motorisation) {
		getMotorisations().remove(motorisation);
		motorisation.setCarburant(null);

		return motorisation;
	}

}