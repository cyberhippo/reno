package model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the transmission database table.
 * 
 */
@Entity
@NamedQuery(name="Transmission.findAll", query="SELECT t FROM Transmission t")
public class Transmission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer transmissionid;

	private Integer prix;

	private String type;

	//bi-directional many-to-one association to Motorisation
	@JsonIgnore
	@OneToMany(mappedBy="transmission")
	private List<Motorisation> motorisations;

	public Transmission() {
	}

	public Integer getTransmissionid() {
		return this.transmissionid;
	}

	public void setTransmissionid(Integer transmissionid) {
		this.transmissionid = transmissionid;
	}

	public Integer getPrix() {
		return this.prix;
	}

	public void setPrix(Integer prix) {
		this.prix = prix;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Motorisation> getMotorisations() {
		return this.motorisations;
	}

	public void setMotorisations(List<Motorisation> motorisations) {
		this.motorisations = motorisations;
	}

	public Motorisation addMotorisation(Motorisation motorisation) {
		getMotorisations().add(motorisation);
		motorisation.setTransmission(this);

		return motorisation;
	}

	public Motorisation removeMotorisation(Motorisation motorisation) {
		getMotorisations().remove(motorisation);
		motorisation.setTransmission(null);

		return motorisation;
	}

}