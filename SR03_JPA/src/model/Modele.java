package model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the modele database table.
 * 
 */
@Entity
@NamedQuery(name="Modele.findAll", query="SELECT m FROM Modele m")
public class Modele implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer idmodele;

	private String marque;

	private double prixbase;

	private String type;

	//bi-directional many-to-one association to Configuration
	@JsonIgnore
	@OneToMany(mappedBy="modele")
	private List<Configuration> configurations;

	//bi-directional many-to-one association to Finition
	@ManyToOne
	@JoinColumn(name="finitionsid")
	private Finition finition;

	//bi-directional many-to-one association to Motorisation
	@ManyToOne
	@JoinColumn(name="motorisationid")
	private Motorisation motorisation;

	//bi-directional many-to-many association to Optionsupplementaire
	@JsonIgnore
	@ManyToMany(mappedBy="modeles")
	private List<Optionsupplementaire> optionsupplementaires;

	public Modele() {
	}

	public Integer getIdmodele() {
		return this.idmodele;
	}

	public void setIdmodele(Integer idmodele) {
		this.idmodele = idmodele;
	}

	public String getMarque() {
		return this.marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public double getPrixbase() {
		return this.prixbase;
	}

	public void setPrixbase(double prixbase) {
		this.prixbase = prixbase;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Configuration> getConfigurations() {
		return this.configurations;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}

	public Configuration addConfiguration(Configuration configuration) {
		getConfigurations().add(configuration);
		configuration.setModele(this);

		return configuration;
	}

	public Configuration removeConfiguration(Configuration configuration) {
		getConfigurations().remove(configuration);
		configuration.setModele(null);

		return configuration;
	}

	public Finition getFinition() {
		return this.finition;
	}

	public void setFinition(Finition finition) {
		this.finition = finition;
	}

	public Motorisation getMotorisation() {
		return this.motorisation;
	}

	public void setMotorisation(Motorisation motorisation) {
		this.motorisation = motorisation;
	}

	public List<Optionsupplementaire> getOptionsupplementaires() {
		return this.optionsupplementaires;
	}

	public void setOptionsupplementaires(List<Optionsupplementaire> optionsupplementaires) {
		this.optionsupplementaires = optionsupplementaires;
	}

}