package model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the configuration database table.
 * 
 */
@Entity
@NamedQuery(name="Configuration.findAll", query="SELECT c FROM Configuration c")
public class Configuration implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer idconfiguration;

	private String url;

	//bi-directional many-to-one association to Choixvoiture
	@JsonIgnore
	@OneToMany(mappedBy="configuration")
	private List<Choixvoiture> choixvoitures;

	//bi-directional many-to-one association to Couleur
	@ManyToOne
	@JoinColumn(name="idcouleur")
	private Couleur couleur;

	//bi-directional many-to-one association to Jante
	@ManyToOne
	@JoinColumn(name="idjante")
	private Jante jante;

	//bi-directional many-to-one association to Modele
	@ManyToOne
	@JoinColumn(name="idmodele")
	private Modele modele;

	public Configuration() {
	}

	public Integer getIdconfiguration() {
		return this.idconfiguration;
	}

	public void setIdconfiguration(Integer idconfiguration) {
		this.idconfiguration = idconfiguration;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Choixvoiture> getChoixvoitures() {
		return this.choixvoitures;
	}

	public void setChoixvoitures(List<Choixvoiture> choixvoitures) {
		this.choixvoitures = choixvoitures;
	}

	public Choixvoiture addChoixvoiture(Choixvoiture choixvoiture) {
		getChoixvoitures().add(choixvoiture);
		choixvoiture.setConfiguration(this);

		return choixvoiture;
	}

	public Choixvoiture removeChoixvoiture(Choixvoiture choixvoiture) {
		getChoixvoitures().remove(choixvoiture);
		choixvoiture.setConfiguration(null);

		return choixvoiture;
	}

	public Couleur getCouleur() {
		return this.couleur;
	}

	public void setCouleur(Couleur couleur) {
		this.couleur = couleur;
	}

	public Jante getJante() {
		return this.jante;
	}

	public void setJante(Jante jante) {
		this.jante = jante;
	}

	public Modele getModele() {
		return this.modele;
	}

	public void setModele(Modele modele) {
		this.modele = modele;
	}

}