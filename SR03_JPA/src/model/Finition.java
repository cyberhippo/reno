package model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the finitions database table.
 * 
 */
@Entity
@Table(name="finitions")
@NamedQuery(name="Finition.findAll", query="SELECT f FROM Finition f")
public class Finition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer finitionsid;

	private Integer prix;

	private String type;

	//bi-directional many-to-one association to Modele
	@JsonIgnore
	@OneToMany(mappedBy="finition")
	private List<Modele> modeles;

	public Finition() {
	}

	public Integer getFinitionsid() {
		return this.finitionsid;
	}

	public void setFinitionsid(Integer finitionsid) {
		this.finitionsid = finitionsid;
	}

	public Integer getPrix() {
		return this.prix;
	}

	public void setPrix(Integer prix) {
		this.prix = prix;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Modele> getModeles() {
		return this.modeles;
	}

	public void setModeles(List<Modele> modeles) {
		this.modeles = modeles;
	}

	public Modele addModele(Modele modele) {
		getModeles().add(modele);
		modele.setFinition(this);

		return modele;
	}

	public Modele removeModele(Modele modele) {
		getModeles().remove(modele);
		modele.setFinition(null);

		return modele;
	}

}