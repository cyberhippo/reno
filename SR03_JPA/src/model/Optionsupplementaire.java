package model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the optionsupplementaire database table.
 * 
 */
@Entity
@NamedQuery(name="Optionsupplementaire.findAll", query="SELECT o FROM Optionsupplementaire o")
public class Optionsupplementaire implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer idoption;

	private String nom;

	private Integer prix;

	//bi-directional many-to-many association to Modele
	@JsonIgnore
	@ManyToMany
	@JoinTable(
		name="ajouteoption"
		, joinColumns={
			@JoinColumn(name="idoption")
			}
		, inverseJoinColumns={
			@JoinColumn(name="idmodele")
			}
		)
	private List<Modele> modeles;

	public Optionsupplementaire() {
	}

	public Integer getIdoption() {
		return this.idoption;
	}

	public void setIdoption(Integer idoption) {
		this.idoption = idoption;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getPrix() {
		return this.prix;
	}

	public void setPrix(Integer prix) {
		this.prix = prix;
	}

	public List<Modele> getModeles() {
		return this.modeles;
	}

	public void setModeles(List<Modele> modeles) {
		this.modeles = modeles;
	}

}