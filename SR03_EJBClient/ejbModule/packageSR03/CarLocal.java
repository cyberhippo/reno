package packageSR03;

import javax.ejb.Local;

import java.util.List;

import model.*;

@Local
public interface CarLocal {
	
   	public List<Jante> getJantes();
   	public Jante getJanteById(int jid);
   	
   	public List<Couleur> getCouleurs();
   	public Couleur getCouleurById(int cid);
   	public void addCouleur(Couleur c);
   	
	public List<Finition> getFinitions();
	public Finition getFinitionById(int fid);
	
	public List<Transmission> getTransmissions();
	public Transmission getTransmissionById(int tid);
	
	public List<Boitedevitesse> getBoitesDeVitesse();
	public Boitedevitesse getBoitedevitesseById(int bdvid);
	
	public List<Carburant> getCarburants();
	public Carburant getCarburantById(int cid);
	
	public List<Optionsupplementaire> getOptionSupplementaires();
	public Optionsupplementaire getOptionsupplementaireById(int osid);
	
	public List<Motorisation> getMotorisations();
   	public Motorisation getMotorisationById(int mid);
   	
	public List<Modele> getModeles();
	public Modele getModeleById(int mid);
	
	public List<Configuration> getConfigurations();
	public Configuration getConfigurationById(int cid);

	
	public List<Choixvoiture> getVoiture();
	public Choixvoiture getVoitureById(int cid);


}
