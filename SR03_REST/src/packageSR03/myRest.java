package packageSR03;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

	
@Stateless
@Path("/voiture")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
public class myRest {
	@EJB
	private CarLocal voiture;
	
	
	@GET
	@Path("/jantes")
    public Response getJantes()
    {
		return Response.ok(this.voiture.getJantes()).build();
    }
	
	@GET
	@Path("/jante/{i}")
    public Response getJanteById( @PathParam("i") int jid) 
	{
		return Response.ok(this.voiture.getJanteById(jid)).build();
	}
	
	@GET
	@Path("/couleurs")
    public Response getCouleurs()
    {
		return Response.ok(this.voiture.getCouleurs()).build();
    }
	
	@GET
	@Path("/couleur/{i}")
    public Response getCouleurById( @PathParam("i") int cid) 
	{
		return Response.ok(this.voiture.getCouleurById(cid)).build();
	}
	
	@GET
	@Path("/finitions")
    public Response getFinition()
    {
		return Response.ok(this.voiture.getFinitions()).build();
    }
	
	@GET
	@Path("/finition/{i}")
    public Response getFinitionById( @PathParam("i") int fid) 
	{
		return Response.ok(this.voiture.getFinitionById(fid)).build();
	}
		
	@GET
	@Path("/transmissions")
    public Response getTransmissions()
    {
		return Response.ok(this.voiture.getTransmissions()).build();
    }
	
	@GET
	@Path("/transmission/{i}")
    public Response getTransmissionById( @PathParam("i") int tid) 
	{
		return Response.ok(this.voiture.getTransmissionById(tid)).build();
	}
	
	@GET
	@Path("/boitesdevitesse")
    public Response getBoitesDeVitesse()
    {
		return Response.ok(this.voiture.getBoitesDeVitesse()).build();
    }
	
	@GET
	@Path("/boitedevitesse/{i}")
    public Response getBoitedevitesseById( @PathParam("i") int bdvid) 
	{
		return Response.ok(this.voiture.getBoitedevitesseById(bdvid)).build();
	}
	
	@GET
	@Path("/carburants")
    public Response getCarburants()
    {
		return Response.ok(this.voiture.getCarburants()).build();
    }
	
	@GET
	@Path("/carburant/{i}")
    public Response getCarburantById( @PathParam("i") int cid) 
	{
		return Response.ok(this.voiture.getCarburantById(cid)).build();
	}
	
	@GET
	@Path("/options")
    public Response getOptionSupplementaires()
    {
		return Response.ok(this.voiture.getOptionSupplementaires()).build();
    }
	
	@GET
	@Path("/option/{i}")
    public Response getOptionSupplementaireById( @PathParam("i") int osid) 
	{
		return Response.ok(this.voiture.getOptionsupplementaireById(osid)).build();
	}
	
	@GET
	@Path("/motorisations")
    public Response getMotorisations()
    {
		return Response.ok(this.voiture.getMotorisations()).build();
    }
	
	@GET
	@Path("/motorisation/{i}")
    public Response getMotorisationById( @PathParam("i") int mid) 
	{
		return Response.ok(this.voiture.getMotorisationById(mid)).build();
	}
	
	@GET
	@Path("/modeles")
    public Response getModeles()
    {
		return Response.ok(this.voiture.getModeles()).build();
    }
	
	@GET
	@Path("/modele/{i}")
    public Response getModelById( @PathParam("i") int mid) 
	{
		return Response.ok(this.voiture.getModeleById(mid)).build();
	}
	
	@GET
	@Path("/configurations")
    public Response getConfigurations()
    {
		return Response.ok(this.voiture.getConfigurations()).build();
    }
	
	@GET
	@Path("/configuration/{i}")
    public Response getConfigurationById( @PathParam("i") int cid) 
	{
		return Response.ok(this.voiture.getConfigurationById(cid)).build();
	}
	
	@GET
	@Path("/choixVoitures")
    public Response getVoiture()
    {
		return Response.ok(this.voiture.getVoiture()).build();
    }
	
	@GET
	@Path("/choix/{i}")
    public Response getVoitureById( @PathParam("i") int cid) 
	{
		return Response.ok(this.voiture.getVoitureById(cid)).build();
	}
	
	
}